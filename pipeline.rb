#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'
require 'date'
require 'mongo'

class Pipeline

    def handle(data)        
         unless @next.nil? 
             @next.handle(data)             
         else 
             data
         end    
    end    

    def >>(next_handler)
        if @next.nil? 
            @next = next_handler
        else 
            @next >> next_handler 
        end
        self
    end    

end    

class ProfileSnapshotTransformer < Pipeline

    def initialize(sid)
        @sid = sid
    end    

    def handle(data)
        if data.kind_of?(Array)
            t_data = data.map do |v|
                make_profile_snapshot(v)
            end    
        else    
            t_data = make_profile_snapshot(data)        
        end
        super(t_data)
    end    

    private
    def make_profile_snapshot(data)
        {"sid" => @sid, "date" => Time.now.strftime("%d/%m/%Y %H:%M"), "object" => data}
    end    
end    

class MongoStore < Pipeline
    include Mongo

    def initialize(db_name, collection_name)
        mongo = MongoClient.new
        db = mongo.db(db_name)
        @collection = db[collection_name]
    end    

    def handle(data)
        if data.kind_of?(Array)
            data.each do |v|
                @collection.insert(v)
            end    
        else
            @collection.insert(data)
        end    
        super(data)
    end    
end
